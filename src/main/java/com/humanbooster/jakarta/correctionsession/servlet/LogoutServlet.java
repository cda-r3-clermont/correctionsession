package com.humanbooster.jakarta.correctionsession.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "logout", value = "/logout")
public class LogoutServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        request.getSession().invalidate();

        PrintWriter out = response.getWriter();
        out.println("<html>" +
                "<meta charset='utf8'/>" +
                "<body>" +
                "<h1>Au revoir</h1>" +
                "<a href='/correction-session/login'>Login</body>");
    }
}
