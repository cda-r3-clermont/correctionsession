package com.humanbooster.jakarta.correctionsession.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "loginServlet", value = "/login")
public class LoginServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Me connecter !</h1>" +
                "<form method='post'>" +
                "<label>Nom d'utilisateur</label>");
        out.println("<input name='username' placeholder='username'>");
        out.println("<input type='submit'>");
        out.println("</body></html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");

        PrintWriter out = response.getWriter();
        out.println("<html>" +
                "<meta charset='utf8'/>" +
                "<body>");
        if(username.isEmpty()){

            out.println("<h1>Me connecter !</h1>" +
                    "<h2>Merci de saisir un username !!! </h2>"+
                    "<form method='post'>" +
                    "<label>Nom d'utilisateur</label>");
            out.println("<input name='username' placeholder='username'>");
            out.println("<input type='submit'>");

        } else {
            request.getSession().setAttribute("username", username);
            out.println("<a href='/correction-session/account'>Accéder à mon espace !</a>");
        }
        out.println("</body></html>");
    }



}
