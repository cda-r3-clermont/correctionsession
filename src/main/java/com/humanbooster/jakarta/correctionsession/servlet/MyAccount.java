package com.humanbooster.jakarta.correctionsession.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "account", value = "/account")
public class MyAccount extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = (String) request.getSession().getAttribute("username");

        PrintWriter out = response.getWriter();
        if(username == null){

            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

            out.println("<html>" +
                    "<meta charset='utf8'/>" +
                    "<body>" +
                    "<h1>Acces impossible veuillez vous connecter</h1>" +
                    "<a href='/correction-session/login'>Login</body></html>");
        } else {
            out.println("<html>" +
                    "<meta charset='utf8'/><body>");
            out.println("<h1>Bonjour "+ username+"</h1>");
            out.println("<a href='/correction-session/logout'>Me déconnecter</a></body></html>");
        }


    }
}
